<?php 

    class Vehicle 
    {

        public $vehicle_name;
        private $_tireCount = 0;
        private $_price = 0;

        public function __construct($tire_count, $vehicleName)  
        {
            $this->_tireCount = $tire_count;
            $this->vehicle_name = $vehicleName;
        }

        public function HasTires() 
        {
            return $this->tireCount > 0;
        }

        public function CalculateTireCount($multiplier) 
        {           
            $this->_price = $multiplier * $this->_tireCount;
            return $this;    
        }

        public function SetTireCount($tire_count) 
        {
            $this->_tireCount = $tire_count;
            return $this;
        }

        public function SetVehicleName($vehicle_name) 
        {
            $this->vehicle_name = $vehicle_name;
            return $this;
        }

        public function GetPrice() 
        {
            return $this->_price;            
        }

    }
