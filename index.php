<?php 

    // int = 4
    // double, float = 12.888888 
    // string "test"
    // Object Vehicle 

    require_once("Vehicle.php");

    $vehicle = new Vehicle(4, "BMW");
    echo "<div><h2>Name: " . $vehicle->vehicle_name . ", Price: " . $vehicle->GetPrice() . "</h2></div>";

    $vehicle->SetVehicleName("Audi");
    $vehicle->SetTireCount(8);
    $vehicle->CalculateTireCount(32);
    
    // Salvesta andmebaasi

    echo "<div><h2>Name: " . $vehicle->vehicle_name . ", Price: " . $vehicle->GetPrice() . "</h2></div>";

    /*
    $x = 10;
    $y = 5;

    $xx = $x + $y;
    */

?>